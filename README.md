# coq-bytestring

More efficient representation of strings using Coq's Byte type.

This type maintains all of the computational properties of the Coq String
datatype and exposes essentially the same interface to ease porting of
libraries.
